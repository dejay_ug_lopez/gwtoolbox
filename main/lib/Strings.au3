

Global Const $GWToolbox = "GWToolbox"
Global Const $DataFolder = @AppDataDir&"\"&$GWToolbox&"\"
Global Const $iniFileName = $GWToolbox&".ini"
Global Const $iniFullPath = $DataFolder&$iniFileName
Global Const $licenseFullPath = $DataFolder&"license.lic"
Global Const $keysIniFullPath = $DataFolder&"keys.ini"
Global Const $tmpFullPath = $DataFolder&"tmp"

Global Const $Host = "http://fbgmguild.com/GWToolbox/"

; Various
Global Const $s_space = " "
Global Const $s_empty = ""
Global Const $s_pipe = "|"
Global Const $s_tilde = "~"
Global Const $s_colon = ":"
Global Const $s_GUIOnEventMode = "GUIOnEventMode"

; Function names
; DO NOT DO THAT - OR OBFUSCATOR WILL FUCK UP


; Buttons
Global Const $s_Cancel = "Cancel"
Global Const $s_Activate = "Activate"
Global Const $s_Delete = "Delete"
Global Const $s_Save = "Save"
Global Const $s__Ok = "Ok"
Global Const $s_No = "No"
Global Const $s_Yes = "Yes"
Global Const $s_Disabled = "Disabled"

; True/false
Global Const $s_True = "True"
Global Const $s_False = "False"

; numbers
Global Const $s_00 = "00"
Global Const $s_0 = "0"
Global Const $s_1 = "1"
Global Const $s_2 = "2"
Global Const $s_3 = "3"
Global Const $s_4 = "4"
Global Const $s_5 = "5"
Global Const $s_6 = "6"
Global Const $s_7 = "7"
Global Const $s_8 = "8"

; dllstructgetdata
Global Const $s_ModelID = "ModelID"
Global Const $s_ID = "ID"
Global Const $s_SkillID = "SkillID"
Global Const $s_Quantity = "Quantity"

; letters lol
Global Const $s_x = "x"
Global Const $s_y = "y"

; ini elements
Global Const $s_sfmacro = "sfmacro"
Global Const $s_builds = "builds"
Global Const $s_pcons = "pcons"

; hotkeys
Global Const $s_stuck = "stuck"
Global Const $s_recall = "recall"
Global Const $s_ua = "ua"
Global Const $s_hidegw = "hidegw"
Global Const $s_resign = "resign"
Global Const $s_clicker = "clicker"
Global Const $s_teamresign = "teamresign"
Global Const $s_res = "res"
Global Const $s_age = "age"
Global Const $s_agepm = "agepm"
Global Const $s_pstone = "pstone"
Global Const $s_focus = "focus"
Global Const $s_looter = "looter"
Global Const $s_identifier = "identifier"
Global Const $s_rupt = "rupt"
Global Const $s_movement = "movement"
Global Const $s_ghostpop = "ghostpop"
Global Const $s_ghosttarget = "ghosttarget"
Global Const $s_gstonepop = "gstonepop"
Global Const $s_legiopop = "legiopop"
Global Const $s_rainbowuse = "rainbowpop"

Global Const $s_DialogHK = "DialogHK"
Global Const $s_number = "number"

; pcons
Global Const $s_cons = "cons"
Global Const $s_alcohol = "alcohol"
Global Const $s_RRC = "RRC"
Global Const $s_BRC = "BRC"

Global Const $s_disclaimer = "DISCLAIMER:"&@CRLF& _
	'THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF ' & _
	'FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY ' & _
	'DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.' & @CRLF&@CRLF & _
	'By clicking the OK button you agree with all the above.'
; ini
Global Const $s_slashkeysini = "\keys.ini"
Global Const $s_keysini = "keys.ini"
Global Const $s_idToKey = "idToKey"

Global Const $s_gwtoolboxcurrentversion = "GWToolbox_currentVersion.txt"
Global Const $s_tmp = "tmp"
Global Const $s_timer = "timer"
Global Const $s_color = "color"
Global Const $s_health = "health"
Global Const $s_party = "party"
Global Const $s_buffs = "buffs"
Global Const $s_distance = "distance"
Global Const $s_Transparency = "Transparency"
Global Const $s_successful_launch_again = "Successful, please launch GWToolbox again"
Global Const $s_error_try_again = "Error during the activation, please try again"
Global Const $s_licenselic = "license.lic"
Global Const $s_activating = "Activating..."
Global Const $s_license = "license/"
Global Const $s_dotlic = ".lic"
Global Const $s_dash_Activation = " - Activation"
Global Const $s_enter_code = "Enter code:"
Global Const $s_Active = "Active"
Global Const $s_Private = "Private"
Global Const $s_privateVersionTxt = "privateVersion.txt"
Global Const $s_error_cannot_attach_to_gw = "Error: Unable to attach GWToolbox to Guild Wars"
Global Const $s_error_not_logged_with_active_license = "ERROR: You are not logged with any character that has an active "&$GWToolbox&" license"
Global Const $s_error_failed_convert_license = "ERROR: Failed to convert licence string. Error code = "
Global Const $s_error_failed_read_license = "ERROR: Failed to read License. Error code = "
Global Const $s_error_failed_read_file = "ERROR: Unable to read License file"
Global Const $s_demo_mode = "The program will run in demo mode and quit in 100 seconds"
Global Const $s_license_not_found = "License not found, do you want to insert a code?"
Global Const $s_error_toolbox_running = "Error: GWToolbox is already running"
Global Const $s_error_gw_not_running = "Error: Guild Wars is not running"
Global Const $s_dotexe = ".exe"
Global Const $s_gwexe = "gw"&$s_dotexe
Global Const $s_press_any_key = "Press any key"
Global Const $s_hotkey = "hotkey"
Global Const $s_active_l = "active"
Global Const $s_pconsToggle = $s_pcons&"Toggle"
Global Const $s_emobondsmonitorhelptext = 	"It will show which bonds are used on each party member."&@CRLF& _
											"Place the monitor next to the party window, it will have the same height."&@CRLF& _
											@CRLF& _
											"Notes:"&@CRLF& _
											"- It will work properly only in parties with no heroes or henchmen"&@CRLF& _
											"- It will only show the bonds you are maintaining"&@CRLF& _
											"- It is not currently possible to remove bonds from the monitor. That might be added on a future update"